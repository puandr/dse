package api;

import dto.TimeZoneDto;
import dto.WeatherDto;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import parser.JSONParser;
import parser.XMLParser;
import service.TimeZoneService;
import service.WeatherService;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/api/dse")
public class dseServlet extends HttpServlet {
    private String xmlResponse;
    private String jsonResponse;
    private String input;
    private String requestContentType;

//    private static final Logger logger = LogManager.getLogger(dseServlet.class);

    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WeatherDto weatherReport = new WeatherDto();
        String cityCode;

        requestContentType = request.getContentType();

        if (requestContentType.equals("application/json")) {
            input = Util.readStream(request.getInputStream());

            cityCode = JSONParser.getValueFromJson(input, "cityCode");

            if (cityCode != null) {
                weatherReport = WeatherService.currentWeather(cityCode);

                jsonResponse = JSONParser.objectToJsonString(weatherReport);

                response.setHeader("Content-Type","application/json");
                response.setStatus(200);
                response.getWriter().println(jsonResponse);
            } else {
                response.setHeader("Content-Type", "text");
                response.setStatus(400);
                response.getWriter().println("wrong JSON format");
            }
        //TODO implement XML validation for empty string, null, correct XML formatting
        } else if (requestContentType.equals("text/xml") || requestContentType.equals("application/xml")) {
            input = Util.readStream(request.getInputStream());

            cityCode = XMLParser.getCityCodeFromXmlRequest(input);

            weatherReport = WeatherService.currentWeather(cityCode);

            xmlResponse = XMLParser.objectToXml(weatherReport);
            response.setHeader("Content-Type","text/xml");
            response.setStatus(200);
            response.getWriter().println(xmlResponse);
        } else {
            response.setHeader("Content-Type", "text");
            response.setStatus(400);
            response.getWriter().println("wrong Content Type in request");
        }
    }

    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TimeZoneDto timeZone = new TimeZoneDto();
        String zipCode;

        requestContentType = request.getContentType();

        if (requestContentType.equals("application/json")) {
            input = Util.readStream(request.getInputStream());

            zipCode = JSONParser.getValueFromJson(input, "zipCode");

            if (zipCode != null) {
                timeZone = TimeZoneService.getTimeZoneByZipCode(zipCode);

                jsonResponse = JSONParser.objectToJsonString(timeZone);

                response.setHeader("Content-Type","application/json");
                response.setStatus(200);
                response.getWriter().println(jsonResponse);
            } else {
                response.setHeader("Content-Type", "text");
                response.setStatus(400);
                response.getWriter().println("wrong JSON format");
            }

         //TODO implement XML validation for empty string, null, correct XML formatting
        } else if (requestContentType.equals("text/xml") || requestContentType.equals("application/xml")) {
            input = Util.readStream(request.getInputStream());

            zipCode = XMLParser.getZipCodeFromXmlRequest(input);

            timeZone = TimeZoneService.getTimeZoneByZipCode(zipCode);

            xmlResponse = XMLParser.objectToXml(timeZone);

            response.setHeader("Content-Type","text/xml");
            response.setStatus(200);
            response.getWriter().println(xmlResponse);
        } else {
            response.setStatus(400);
            response.getWriter().println("wrong Content Type in request");
        }
    }
}
