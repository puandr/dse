package dto;

import lombok.Data;

@Data
public class TimeZoneDto {
    private String zipCode;
    private String timeZone;
}
