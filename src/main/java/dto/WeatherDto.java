package dto;

import lombok.Data;

@Data
public class WeatherDto {
    private String cityName;
    private String cityCode;
    private Double temp;
    private int pressure;
    private int humidity;
}
