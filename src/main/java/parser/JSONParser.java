package parser;

import api.dseServlet;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

    private static final Logger logger = LogManager.getLogger(JSONParser.class);

    private static JSONObject jsonObject;

    public static boolean checkForJsonValueValidity (String jsonString, String jsonValue) {
        if (isJSONValid(jsonString)) {
            if (jsonObject.has(jsonValue)) {
                if (!"".equals(jsonObject.getString(jsonValue)) && jsonObject.getString(jsonValue) != null ) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getValueFromJson (String jsonString, String jsonValue){
        //TODO move to constructor or separate method?
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (Exception ex) {
            //System.out.println(ex);
            logger.error("JSONParser:objectToJsonString: trying to make new json ObjectMapper", ex);

            return null;
        }

        if (checkForJsonValueValidity(jsonString, jsonValue)) {
            String value = jsonObject.getString(jsonValue);
            return value;
        }
        return null;
    }

    public static String objectToJsonString (Object inputObject){
        String json = "";

        try {
            json = new ObjectMapper().writeValueAsString(inputObject);
        } catch (Exception ex) {
            //System.out.println(ex);
            logger.error("JSONParser:objectToJsonString: trying to make new json ObjectMapper", ex);
        }
        return json;
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

}
