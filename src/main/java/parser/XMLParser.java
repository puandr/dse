package parser;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import dto.WeatherDto;

public class XMLParser {
    public static String xmlToString (String xml) {
        return "";
    }

    public static String getCityCodeFromXmlRequest (String xml) {

        String cityCodeFromXml = "";
        String cityCode = "";

        cityCodeFromXml = getTagValue(xml, "cityCode");

        cityCode = cityCodeFromXml.replaceAll("[^0-9]","");

        return cityCode;
    }

    public static String getZipCodeFromXmlRequest(String xml){
        String zipCodeFromXml = "";
        String zipCode = "";

        zipCodeFromXml = getTagValue(xml, "zipCode");

        zipCode = zipCodeFromXml.replaceAll("[^0-9]","");

        return zipCode;
    }

    private static String getTagValue(String xml, String tagName){
        return xml.split("<"+tagName+">")[1].split("</"+tagName+">")[0];
    }


    public static String objectToXml (Object object) {
        XmlMapper xmlMapper = new XmlMapper();
        String xml = "";

        try {
            xml = xmlMapper.writeValueAsString(object);
        } catch (Exception ex) {
            //TODO log exception
            System.out.println(ex);
        }

        return xml;
    }

}
