package service;

import dto.TimeZoneDto;

import java.util.Random;

public class TimeZoneService {
    public static TimeZoneDto getTimeZoneByZipCode (String zipCode) {
        int min = -12;
        int max = 12;
        TimeZoneDto timeZone = new TimeZoneDto();
        timeZone.setZipCode(zipCode);

        Random random = new Random();
        int timeZoneValue = random.nextInt(max - min) + min;

        timeZone.setTimeZone(Integer.toString(timeZoneValue));
        return timeZone;
    }
}
