package service;

import dto.WeatherDto;

import java.util.Random;

public class WeatherService {
    private enum cityNames {
        Tallinn,
        Riga,
        Helsinki
    }

    public static WeatherDto currentWeather (String cityCode) {
        Random random = new Random();

        WeatherDto currentWeatherOutput = new WeatherDto();

        currentWeatherOutput.setCityName(cityNames.values()[new Random().nextInt(cityNames.values().length)].toString());
        currentWeatherOutput.setCityCode(cityCode);
        currentWeatherOutput.setTemp(random.nextDouble()*100 - 50);
        currentWeatherOutput.setHumidity(random.nextInt(100));
        currentWeatherOutput.setPressure((random.nextInt(200)+900));

        return currentWeatherOutput;
    }
}
